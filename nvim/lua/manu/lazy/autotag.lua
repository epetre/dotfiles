return {
  'windwp/nvim-ts-autotag',
  config = function() 
    require('nvim-ts-autotag').setup({
      aliases = {
        ["your_language_here"] = "html",
      }
    })
  end
}
