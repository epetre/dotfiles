return {
    "neovim/nvim-lspconfig",
    lazy = false,
    dependencies = {
        "williamboman/mason.nvim",
        "williamboman/mason-lspconfig.nvim",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-path",
        "hrsh7th/cmp-cmdline",
        "hrsh7th/nvim-cmp",
        "L3MON4D3/LuaSnip",
        "saadparwaiz1/cmp_luasnip",
        "j-hui/fidget.nvim",
        -- Only Solargraph for Ruby
    },
    config = function()
        local cmp = require('cmp')
        local cmp_lsp = require("cmp_nvim_lsp")
        local lsp_config = require("lspconfig")
        local capabilities = vim.tbl_deep_extend(
            "force",
            {},
            vim.lsp.protocol.make_client_capabilities(),
            cmp_lsp.default_capabilities())

        -- Solargraph setup
        lsp_config.solargraph.setup {
            capabilities = capabilities,
            settings = {
                solargraph = {
                    diagnostics = true,
                    formatting = true,
                }
            }
        }

        -- Dart setup
        lsp_config.dartls.setup {
            cmd = { "dart", "language-server", "--protocol=lsp" },
            capabilities = capabilities,
            root_dir = lsp_config.util.root_pattern("pubspec.yaml", ".git"),
        }

        -- Other existing LSP configurations...
        lsp_config.lua_ls.setup {
            capabilities = capabilities,
            settings = {
                Lua = {
                    diagnostics = {
                        -- globals = { "vim" }
                    }
                }
            }
        }

        -- Diagnostics configuration
        vim.diagnostic.config({
            virtual_text = false,  -- Show virtual text for diagnostics
            signs = true,
            underline = true,
            update_in_insert = false,
            float = {
                focusable = false,
                style = "minimal",
            },
        })

        -- Key mappings for toggling diagnostics
        vim.api.nvim_set_keymap('n', '<leader>i', ':lua vim.diagnostic.open_float()<CR>', { noremap = true, silent = true })  -- Toggle diagnostics
        vim.api.nvim_set_keymap('n', '[d', ':lua vim.diagnostic.goto_prev()<CR>', { noremap = true, silent = true })  -- Go to previous diagnostic
        vim.api.nvim_set_keymap('n', ']d', ':lua vim.diagnostic.goto_next()<CR>', { noremap = true, silent = true })  -- Go to next diagnostic

        -- Key mapping for commenting lines
        vim.api.nvim_set_keymap('n', '<leader>c', ':lua require("Comment.api").toggle.linewise.current()<CR>', { noremap = true, silent = true }) -- Toggle comments

        -- Key mappings for LSP navigation
        vim.api.nvim_set_keymap('n', 'gd', ':lua vim.lsp.buf.definition()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'gD', ':lua vim.lsp.buf.declaration()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'gr', ':lua vim.lsp.buf.references()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'gt', ':lua vim.lsp.buf.type_definition()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'rn', ':lua vim.lsp.buf.rename()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'K', ':lua vim.lsp.buf.hover()<CR>', { noremap = true, silent = true })
        vim.api.nvim_set_keymap('n', 'gi', ':lua vim.lsp.buf.implementation()<CR>', { noremap = true, silent = true })

        -- Completion setup
        local cmp_select = { behavior = cmp.SelectBehavior.Select }

        cmp.setup({
            snippet = {
                expand = function(args)
                    require('luasnip').lsp_expand(args.body)
                end,
            },
            mapping = cmp.mapping.preset.insert({
                ['<C-p>'] = cmp.mapping.select_prev_item(cmp_select),
                ['<C-n>'] = cmp.mapping.select_next_item(cmp_select),
                ['<C-y>'] = cmp.mapping.confirm({ select = true }),
                ["<C-Space>"] = cmp.mapping.complete(),
            }),
            sources = cmp.config.sources({
                { name = 'nvim_lsp' },
                { name = 'luasnip' },
            }, {
                { name = 'buffer' },
            })
        })

        -- Mason setup
        require("mason").setup()
        require("mason-lspconfig").setup({
            ensure_installed = {
                "solargraph", 
                "rust_analyzer",
                "gradle_ls",
                "astro",
                "html",
                "eslint",
                "lua_ls"
            },
        })

        require("fidget").setup({})
    end,
}
