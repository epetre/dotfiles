vim.opt.guicursor = ""

vim.opt.nu = true
vim.opt.relativenumber = true

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true

--vim.opt.textwidth = 80 -- for setting the max textwidth to 80
--vim.opt.wrap = true -- ensures we automatically always wrap lines
-- Show ruler at 80 characters
vim.opt.colorcolumn = "+1"
vim.opt.colorcolumn = "81"

-- Highlight text beyond 80 characters in gray
vim.cmd("highlight OverLength ctermbg=lightgray guibg=lightgray")
vim.cmd("match OverLength /\\%>80v./")


vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50
