call pathogen#infect()
syntax on
filetype plugin indent on
" colors tir_black nice dark and colored smooeth

packadd! dracula
syntax enable
colorscheme dracula

let &t_Co=256
set guifont=Monaco:h14

set number
set relativenumber
set hlsearch
set incsearch

:hi Normal ctermbg=NONE
:hi NonText ctermbg=NONE
:hi LineNr ctermbg=NONE

set autoindent
set expandtab
set shiftwidth=2

" Source the vimrc file after saving it
if has("autocmd")
  autocmd! bufwritepost .vimrc source $MYVIMRC
endif

" Visual
set showmatch  " Show matching brackets.
" Show $ at end of line and trailing space as ~
set lcs=tab:\ \ ,eol:$,trail:~,extends:>,precedes:<
set novisualbell  " No blinking .
set noerrorbells  " No noise.

" The following optional commands are helpful but require explicit creation of
" directories and files:
" Backups & Files
set backup                     " Enable creation of backup file.
set backupdir=~/.vim/backups " Where backups will go.
set directory=~/.vim/tmp     " Where temporary files will go.

" let ctrl_p plugin use wildignore
if exists("g:ctrl_user_command")
    unlet g:ctrlp_user_command
endif

set wildignore+=.*

set path+=**                                                                    
set wildignore+=**/node_modules/** 
set wildignore+=**/platforms/** 
set wildignore+=**/tmp/** 
set wildignore+=**/vendor/** 
set wildignore+=**/cache/** 
