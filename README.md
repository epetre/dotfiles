dotfiles
========

### Sym links
- ln -s ~/bin/dotfiles/.config ~/.config
- ln -sf ~/bin/dotfiles/.vimrc ~/.vimrc  
- ln -sf ~/bin/dotfiles/zshenv ~/.zshenv
- ln -sf ~/bin/dotfiles/zshrc ~/.zshrc
- ln -s ~/bin/dotfiles/zprofile ~/.zprofile
- ln -sf ~/bin/dotfiles/irbrc ~/.irbrc
- ln -s ~/bin/dotfiles/nvim ~/.config/nvim

- ln -s ~/bin/dotfiless/vim ~/.vim
- ln -s ~/.vim/colors/ ~/bin/dotfiles/vim/
- ln -s /.vim/pack/themes/start/ ~/bin/dotfiles/vim/pack/themes/start/


## Neovim
- Plugins install `:Lazy sync`
- Language servers `:Mason`
- Parsers update `:TSUpdate`

## Homebrew tools

╰─ brew ls                                                                          ─╯
==> Formulae
act			jemalloc		neovim
aom			jpeg			netpbm
aribb24			jpeg-turbo		nettle
atk			jpeg-xl			nghttp2
augeas			jq			nmap
autoconf		lame			node
automake		leptonica		oniguruma
awscli			libarchive		opencore-amr
bash-completion		libass			openexr
bdw-gc			libavif			openjdk
berkeley-db		libb2			openjdk@17
boost			libbluray		openjpeg
brotli			libev			openldap
bundletool		libevent		openssl@1.1
c-ares			libffi			openssl@3
ca-certificates		libgcrypt		opus
cairo			libgnt			p11-kit
certbot			libgpg-error		pango
cffi			libidn			pcre
cjson			libidn2			pcre2
cmake			libksba			perl
cmocka			liblinear		pixman
coreutils		libmicrohttpd		pkg-config
cscope			libnghttp2		powerlevel10k
curl			libogg			prettyping
curl-ca-bundle		libotr			pycparser
dart			libpng			pyenv
dav1d			libpthread-stubs	pygments
dialog			librist			python@3.10
docutils		librsvg			python@3.11
ffmpeg			libsamplerate		python@3.8
flac			libsndfile		python@3.9
flyctl			libsodium		rav1e
fontconfig		libsoxr			readline
freetype		libssh2			redis
frei0r			libtasn1		ripgrep
fribidi			libtermkey		rtmpdump
fvm			libtiff			rubberband
gd			libtool			ruby
gdbm			libunibreak		sdl2
gdk-pixbuf		libunistring		sdl2_image
gettext			libusb			six
ghostscript		libuv			snappy
giflib			libvidstab		speex
glew			libvmaf			sphinx-doc
glib			libvorbis		sqlite
gmp			libvpx			srt
gnu-sed			libvterm		svt-av1
gnutls			libx11			tcl-tk
go			libxau			tesseract
gobject-introspection	libxcb			theora
gource			libxdmcp		tmux
gradle@7		libxext			tree-sitter
graphite2		libxml2			unbound
graphviz		libxrender		unibilium
gtk+			libyaml			utf8proc
gts			little-cms2		uthash
guile			lua			webp
harfbuzz		lua@5.3			wget
heroku			luajit			x264
heroku-node		luv			x265
hicolor-icon-theme	lz4			xorgproto
hidapi			lzo			xvid
highway			m4			xz
icu4c			mac-cleanup		yabai
imagemagick@6		mbedtls			yarn
imath			mpdecimal		zeromq
jansson			mpg123			zimg
jasper			msgpack			zlib
jbig2dec		ncurses			zstd

==> Casks
xquartz
